#include <ros/ros.h>
#include "std_msgs/String.h"
 
class SubAndPub{
public:
  SubAndPub()
  {
    pub_ = n_.advertise<std_msgs::String>("/sub_and_pub", 1);
    sub_ = n_.subscribe("/message", 1, &SubAndPub::callback, this);
  }
  void callback(const std_msgs::String::ConstPtr& input)
  {
    std_msgs::String output;
    output.data = input->data.c_str();
    pub_.publish(output);
  }
private:
  ros::NodeHandle n_; 
  ros::Publisher pub_;
  ros::Subscriber sub_;
};
 
int main(int argc, char **argv)
{
  ros::init(argc, argv, "sub_and_pub");
  SubAndPub SAPObject;
  ros::spin();
  return 0;
}
