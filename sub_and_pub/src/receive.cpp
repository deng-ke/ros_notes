#include "ros/ros.h"
#include "std_msgs/String.h"

void messageCallback(const std_msgs::String::ConstPtr& msg)
{
  ROS_INFO("I heard: [%s]", msg->data.c_str());
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "receive");
  ros::NodeHandle n;
  ros::Subscriber sub = n.subscribe("sub_and_pub", 1000, messageCallback);
  ros::spin();
  return 0;
}
